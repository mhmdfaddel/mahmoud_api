package com.example.rest_api.freiberufler;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Freiberufler {

        private @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        Long freiberufler_id;
    private float stdSatz;
    private int stunde;

    private String firstname, middlename, lastname;
    private Date arbeitsBeginn;
    private String steuerNummer;


        public Freiberufler() {
        }

    public Long getFreiberufler_id() {
        return freiberufler_id;
    }


    public void setFreiberufler_id(Long freiberufler_id) {
        this.freiberufler_id = freiberufler_id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getArbeitsBeginn() {
        return arbeitsBeginn;
    }

    public void setArbeitsBeginn(Date arbeitsBeginn) {
        this.arbeitsBeginn = arbeitsBeginn;
    }

    public String getSteuerNummer() {
        return steuerNummer;
    }

    public void setSteuerNummer(String steuerNummer) {
        this.steuerNummer = steuerNummer;
    }

    public float getStdSatz() {
        return stdSatz;
    }

    public void setStdSatz(float stdSatz) {
        this.stdSatz = stdSatz;
    }

    public int getStunde() {
        return stunde;
    }

    public void setStunde(int stunde) {
        this.stunde = stunde;
    }
}