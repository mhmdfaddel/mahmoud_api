package com.example.rest_api.freiberufler;

class FreiberuflerNotFoundException extends RuntimeException {

    FreiberuflerNotFoundException(Long id) {
        super("Could not find Freiberufler " + id);
    }
}