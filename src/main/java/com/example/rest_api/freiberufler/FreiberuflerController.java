package com.example.rest_api.freiberufler;

import com.example.rest_api.mitarbeiter.Mitarbeiter;
import com.example.rest_api.mitarbeiter.MitarbeiterRepo;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class FreiberuflerController {

    private final FreiberuflerRepo repository;
    private final MitarbeiterRepo mitarbeiterrepository;

    FreiberuflerController(FreiberuflerRepo repository, MitarbeiterRepo mitarbeiterrepository) {
        this.repository = repository;
        this.mitarbeiterrepository = mitarbeiterrepository;
    }

    @GetMapping("/freiberufler")
    List<Freiberufler> all() {
        return repository.findAll();
    }


    @PostMapping("/freiberufler")
    String newFreiberufler(@RequestBody Freiberufler newFreiberufler) {
        Mitarbeiter mitarbeiter = new Mitarbeiter();
        mitarbeiter.setTypus("freiberufler");
        mitarbeiter.setSteuerNummer(newFreiberufler.getSteuerNummer());
        mitarbeiter.setArbeitsBeginn(newFreiberufler.getArbeitsBeginn());
        mitarbeiter.setFirstname(newFreiberufler.getFirstname());
        mitarbeiter.setMiddlename(newFreiberufler.getMiddlename());
        mitarbeiter.setLastname(newFreiberufler.getLastname());
        mitarbeiterrepository.save(mitarbeiter);

        repository.save(newFreiberufler);
        return "freiberufler was added Successfully";
    }

    // Single item

    @GetMapping("/freiberufler/{id}")
    Freiberufler one(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new FreiberuflerNotFoundException(id));
    }

    @PutMapping("/freiberufler/{id}")
    Freiberufler replaceFreiberufler(@RequestBody Freiberufler newFreiberufler, @PathVariable Long id) {

        return repository.findById(id).map(freiberufler -> {
            freiberufler.setArbeitsBeginn(newFreiberufler.getArbeitsBeginn());
            freiberufler.setFirstname(newFreiberufler.getFirstname());
            freiberufler.setMiddlename(newFreiberufler.getMiddlename());
            freiberufler.setLastname(newFreiberufler.getLastname());
            freiberufler.setSteuerNummer(newFreiberufler.getSteuerNummer());
            freiberufler.setStdSatz(newFreiberufler.getStdSatz());
            freiberufler.setStunde(newFreiberufler.getStunde());
            return repository.save(freiberufler);
        }).orElseGet(() -> {
            newFreiberufler.setFreiberufler_id(id);
            return repository.save(newFreiberufler);
        });
    }

    @DeleteMapping("/freiberufler/{id}")
    void deleteFreiberufler(@PathVariable Long id) {
        repository.deleteById(id);
    }
}