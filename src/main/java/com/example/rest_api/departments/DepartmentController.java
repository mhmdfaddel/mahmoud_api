package com.example.rest_api.departments;

import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class DepartmentController {

    private final DepartmentRepo repository;

    DepartmentController(DepartmentRepo repository) {
        this.repository = repository;
    }


    @GetMapping("/department")
    List<Department> all() {
        return repository.findAll();
    }


    @PostMapping("/department")
    String newDepartment(@RequestBody Department newDepartment) {
        repository.save(newDepartment);
        return "Department was added Successfully";
    }


    @GetMapping("/department/{id}")
    Department one(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new DepartmentNotFoundException(id));
    }

    @PutMapping("/department/{id}")
    Department replaceDepartment(@RequestBody Department newDepartment, @PathVariable Long id) {

        return repository.findById(id).map(department -> {
            department.setDep_manager(newDepartment.getDep_manager());
            department.setDep_name(newDepartment.getDep_name());
            return repository.save(department);
        }).orElseGet(() -> {
            newDepartment.setDepartment_id(id);
            return repository.save(newDepartment);
        });
    }

    @DeleteMapping("/department/{id}")
    void deleteDepartment(@PathVariable Long id) {
        repository.deleteById(id);
    }
}