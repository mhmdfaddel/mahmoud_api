package com.example.rest_api.departments;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Department {

        private @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        Long department_id;
        private String dep_manager, dep_name;

        public Department() {
        }

    public Long getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(Long department_id) {
        this.department_id = department_id;
    }

    public String getDep_manager() {
        return dep_manager;
    }

    public void setDep_manager(String dep_manager) {
        this.dep_manager = dep_manager;
    }

    public String getDep_name() {
        return dep_name;
    }

    public void setDep_name(String dep_name) {
        this.dep_name = dep_name;
    }
}