package com.example.rest_api.departments;

class DepartmentNotFoundException extends RuntimeException {

    DepartmentNotFoundException(Long id) {
        super("Could not find Department " + id);
    }
}