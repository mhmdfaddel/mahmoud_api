package com.example.rest_api.manager;

import com.example.rest_api.mitarbeiter.Mitarbeiter;
import com.example.rest_api.mitarbeiter.MitarbeiterRepo;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class ManagerController {

    private final ManagerRepo repository;
    private final MitarbeiterRepo mitarbeiterrepository;

    ManagerController(ManagerRepo repository, MitarbeiterRepo mitarbeiterrepository) {
        this.repository = repository;
        this.mitarbeiterrepository = mitarbeiterrepository;
    }


    @GetMapping("/manager")
    List<Manager> all() {
        return repository.findAll();
    }


    @PostMapping("/manager")
    String newManager(@RequestBody Manager newManager) {
        Mitarbeiter mitarbeiter = new Mitarbeiter();
        mitarbeiter.setTypus("manager");
        mitarbeiter.setSteuerNummer(newManager.getSteuerNummer());
        mitarbeiter.setArbeitsBeginn(newManager.getArbeitsBeginn());
        mitarbeiter.setFirstname(newManager.getFirstname());
        mitarbeiter.setMiddlename(newManager.getMiddlename());
        mitarbeiter.setLastname(newManager.getLastname());
        mitarbeiterrepository.save(mitarbeiter);

        repository.save(newManager);
        return "manager was added Successfully";
    }

    // Single item

    @GetMapping("/manager/{id}")
    Manager one(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new ManagerNotFoundException(id));
    }

    @PutMapping("/manager/{id}")
    Manager replaceManager(@RequestBody Manager newManager, @PathVariable Long id) {

        return repository.findById(id).map(manager -> {
            manager.setArbeitsBeginn(newManager.getArbeitsBeginn());
            manager.setFirstname(newManager.getFirstname());
            manager.setMiddlename(newManager.getMiddlename());
            manager.setLastname(newManager.getLastname());
            manager.setGehalt(newManager.getGehalt());
            manager.setSteuerNummer(newManager.getSteuerNummer());
            manager.setAuto(newManager.getAuto());
            manager.setTel(newManager.getTel());
            return repository.save(manager);
        }).orElseGet(() -> {
            newManager.setManager_id(id);
            return repository.save(newManager);
        });
    }

    @DeleteMapping("/manager/{id}")
    void deleteManager(@PathVariable Long id) {
        repository.deleteById(id);
    }
}