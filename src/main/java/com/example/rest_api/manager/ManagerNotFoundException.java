package com.example.rest_api.manager;

class ManagerNotFoundException extends RuntimeException {

    ManagerNotFoundException(Long id) {
        super("Could not find Department " + id);
    }
}