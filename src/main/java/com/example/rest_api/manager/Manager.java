package com.example.rest_api.manager;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Manager {

        private @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        Long Manager_id;
        private float gehalt;
    private String auto;
    private String tel;
    private String firstname, middlename, lastname;
    private Date arbeitsBeginn;
    private String steuerNummer;


        public Manager() {
        }

    public Long getManager_id() {
        return Manager_id;
    }


    public void setManager_id(Long manager_id) {
        Manager_id = manager_id;
    }

    public void setGehalt(float gehalt) {
        this.gehalt = gehalt;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getArbeitsBeginn() {
        return arbeitsBeginn;
    }

    public void setArbeitsBeginn(Date arbeitsBeginn) {
        this.arbeitsBeginn = arbeitsBeginn;
    }

    public String getSteuerNummer() {
        return steuerNummer;
    }

    public void setSteuerNummer(String steuerNummer) {
        this.steuerNummer = steuerNummer;
    }

    public float getGehalt() {
            return this.gehalt;
        }

    public String getAuto() {
        return auto;
    }

    public void setAuto(String auto) {
        this.auto = auto;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }
}