package com.example.rest_api.mitarbeiter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Mitarbeiter {

        private @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        Long mitarbeiter_id;
    private String firstname, middlename, lastname;
    private Date arbeitsBeginn;
    private String steuerNummer;
    private String typus;
    private String employeeNumber;

    public Mitarbeiter() {}

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getArbeitsBeginn() {
        return arbeitsBeginn;
    }

    public void setArbeitsBeginn(Date arbeitsBeginn) {
        this.arbeitsBeginn = arbeitsBeginn;
    }

    public String getSteuerNummer() {
        return steuerNummer;
    }

    public void setSteuerNummer(String steuerNummer) {
        this.steuerNummer = steuerNummer;
    }

    public Long getMitarbeiter_id() {
        return mitarbeiter_id;
    }

    public void setMitarbeiter_id(Long mitarbeiter_id) {
        mitarbeiter_id = mitarbeiter_id;
    }

    public String getTypus() {
        return typus;
    }

    public void setTypus(String typus) {
        this.typus = typus;
    }

    public String getEmployeeNumber() {
        return employeeNumber;
    }

    public void setEmployeeNumber(String employeeNumber) {
        this.employeeNumber = employeeNumber;
    }
}