package com.example.rest_api.mitarbeiter;

class MitarbeiterNotFoundException extends RuntimeException {

    MitarbeiterNotFoundException(Long id) {
        super("Could not find Mitarbeiter " + id);
    }
}