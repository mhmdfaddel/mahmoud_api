package com.example.rest_api.mitarbeiter;

import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class MitarbeiterController {

    private final MitarbeiterRepo repository;

    MitarbeiterController(MitarbeiterRepo repository) {
        this.repository = repository;
    }

   @GetMapping("/mitarbeiter")
    List<Mitarbeiter> all() {
        return repository.findAll();
    }


    @PostMapping("/mitarbeiter")
    String newmitarbeiter(@RequestBody Mitarbeiter newMitarbeiter) {
        repository.save(newMitarbeiter);
        return "mitarbeiter was added Successfully";
    }


    @GetMapping("/mitarbeiter/{id}")
    Mitarbeiter one(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new MitarbeiterNotFoundException(id));
    }

    @PutMapping("/mitarbeiter/{id}")
    Mitarbeiter replacemitarbeiter(@RequestBody Mitarbeiter newMitarbeiter, @PathVariable Long id) {

        return repository.findById(id).map(mitarbeiter -> {
            mitarbeiter.setArbeitsBeginn(newMitarbeiter.getArbeitsBeginn());
            mitarbeiter.setFirstname(newMitarbeiter.getFirstname());
            mitarbeiter.setMiddlename(newMitarbeiter.getMiddlename());
            mitarbeiter.setLastname(newMitarbeiter.getLastname());
            mitarbeiter.setSteuerNummer(newMitarbeiter.getSteuerNummer());
            mitarbeiter.setTypus(newMitarbeiter.getTypus());
            mitarbeiter.setEmployeeNumber(newMitarbeiter.getEmployeeNumber());
            return repository.save(mitarbeiter);
        }).orElseGet(() -> {
            newMitarbeiter.setMitarbeiter_id(id);
            return repository.save(newMitarbeiter);
        });
    }

    @DeleteMapping("/mitarbeiter/{id}")
    void deletemitarbeiter(@PathVariable Long id) {
        repository.deleteById(id);
    }
}