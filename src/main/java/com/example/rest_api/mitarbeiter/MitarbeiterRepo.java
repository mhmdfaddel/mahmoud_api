package com.example.rest_api.mitarbeiter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public interface MitarbeiterRepo extends JpaRepository<Mitarbeiter, Long> {
}
