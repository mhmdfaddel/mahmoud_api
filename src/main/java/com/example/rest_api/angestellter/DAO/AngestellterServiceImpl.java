package com.example.rest_api.angestellter.DAO;

import com.example.rest_api.angestellter.Angestellter;

import org.springframework.beans.factory.annotation.Autowired;

public class AngestellterServiceImpl implements AngestellterService{

    @Autowired
    AngestellterDAO angestellterDao;


    @Override
    public void insertAngestellter(Angestellter ang) {
       angestellterDao.insertAngestellter(ang);
        
    }

    @Override
    public void deleteAngestellterById(Long angid) {
        angestellterDao.deleteAngestellterById(angid);
        
    }
    
}
