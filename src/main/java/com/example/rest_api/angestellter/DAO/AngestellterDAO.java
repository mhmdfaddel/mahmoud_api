
package com.example.rest_api.angestellter.DAO;

import com.example.rest_api.angestellter.Angestellter;

public interface AngestellterDAO {
    void insertAngestellter(Angestellter ang);

    void deleteAngestellterById(Long angid);
}
