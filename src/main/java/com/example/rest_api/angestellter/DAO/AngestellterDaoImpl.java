import com.example.rest_api.angestellter.Angestellter;



import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;



@Repository
public class AngestellterDaoImpl extends JdbcDaoSupport implements AngestellterDAO {

@Autowired
DataSource dataSource;

@PostConstruct
private void initialize() {
setDataSource(dataSource);
}

@Override
public void insertAngestellter(Angestellter ang) {
String sql = "INSERT INTO employee " + "(angestllter_id, firstname) VALUES (?, ?)";
getJdbcTemplate().update(sql, new Object[] { ang.getAngestllter_id(), ang.getFirstname() });
}

@Override
public void deleteAngestellterById(Long angid) {
String sql = "DELETE FROM angestellter WHERE angestllter_id = ?";
getJdbcTemplate().update(sql, new Object[] { angid });
}
}
