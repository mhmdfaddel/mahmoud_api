package com.example.rest_api.angestellter;

class AngestellterNotFoundException extends RuntimeException {

    AngestellterNotFoundException(Long id) {
        super("Could not find Angetellter " + id);
    }
}