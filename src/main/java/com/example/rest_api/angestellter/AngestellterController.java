package com.example.rest_api.angestellter;

import com.example.rest_api.mitarbeiter.Mitarbeiter;
import com.example.rest_api.mitarbeiter.MitarbeiterRepo;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
class AngestellterController {

    private final AngestellterRepo repository;
    private final MitarbeiterRepo mitarbeiterrepository;

    AngestellterController(AngestellterRepo repository, MitarbeiterRepo repo) {
        this.repository = repository;
        this.mitarbeiterrepository = repo;
    }


    @GetMapping("/angestellter")
    List<Angestellter> all() {
        return repository.findAll();
    }
    // end::get-aggregate-root[]


    @PostMapping("/angestellter")
    String newAngestellter(@RequestBody Angestellter newAngestellter) {
        repository.save(newAngestellter);
        Mitarbeiter mitarbeiter = new Mitarbeiter();
        mitarbeiter.setTypus("angestellter");
        mitarbeiter.setSteuerNummer(newAngestellter.getSteuerNummer());
        mitarbeiter.setArbeitsBeginn(newAngestellter.getArbeitsBeginn());
        mitarbeiter.setFirstname(newAngestellter.getFirstname());
        mitarbeiter.setMiddlename(newAngestellter.getMiddlename());
        mitarbeiter.setLastname(newAngestellter.getLastname());
        mitarbeiterrepository.save(mitarbeiter);
        return "angestellter was added Successfully";
    }

    // Single item

    @GetMapping("/angestellter/{id}")
    Angestellter one(@PathVariable Long id) {
        return repository.findById(id).orElseThrow(() -> new AngestellterNotFoundException(id));
    }

    @PutMapping("/angestellter/{id}")
    Angestellter replaceAngestellter(@RequestBody Angestellter newAngestellter, @PathVariable Long id) {

        return repository.findById(id).map(angestellter -> {
            angestellter.setArbeitsBeginn(newAngestellter.getArbeitsBeginn());
            angestellter.setFirstname(newAngestellter.getFirstname());
            angestellter.setMiddlename(newAngestellter.getMiddlename());
            angestellter.setLastname(newAngestellter.getLastname());
            angestellter.setGehalt(newAngestellter.getGehalt());
            return repository.save(angestellter);
        }).orElseGet(() -> {
            newAngestellter.setAngestllter_id(id);
            return repository.save(newAngestellter);
        });
    }

    @DeleteMapping("/angestellter/{id}")
    void deleteAngestellter(@PathVariable Long id) {
        repository.deleteById(id);
    }
}