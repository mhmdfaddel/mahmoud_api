package com.example.rest_api.angestellter;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Angestellter  {

        private @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        Long angestllter_id;
        private float gehalt;


    private String firstname, middlename, lastname;
    private Date arbeitsBeginn;
    private String steuerNummer;

        public Angestellter() {
        }

    public Long getAngestllter_id() {
        return angestllter_id;
    }


    public void setAngestllter_id(Long angestllter_id) {
        this.angestllter_id = angestllter_id;
    }

    public void setGehalt(float gehalt) {
        this.gehalt = gehalt;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getMiddlename() {
        return middlename;
    }

    public void setMiddlename(String middlename) {
        this.middlename = middlename;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getArbeitsBeginn() {
        return arbeitsBeginn;
    }

    public void setArbeitsBeginn(Date arbeitsBeginn) {
        this.arbeitsBeginn = arbeitsBeginn;
    }

    public String getSteuerNummer() {
        return steuerNummer;
    }

    public void setSteuerNummer(String steuerNummer) {
        this.steuerNummer = steuerNummer;
    }

    public float getGehalt() {
            return this.gehalt;
        }
    }