package com.example.rest_api.angestellter;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AngestellterRepo extends JpaRepository<Angestellter, Long> {
}
