package com.example.rest_api;

import com.example.rest_api.angestellter.Angestellter;
import com.example.rest_api.angestellter.DAO.AngestellterService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;

@SpringBootApplication(exclude = {SecurityAutoConfiguration.class})
public class RestServiceApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
      ApplicationContext context = SpringApplication.run(RestServiceApplication.class, args);

        AngestellterService angestellterService = context.getBean(AngestellterService.class);
        

        Angestellter ange = new Angestellter();
        ange.setAngestllter_id(new Long(3));
        ange.setFirstname("Mahmoud");

        angestellterService.insertAngestellter(ange);


        //angestellterService.deleteAngestellterById(ange.getAngestllter_id());

    }

    protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
        return builder.sources(RestServiceApplication.class);
    }
}
